package sbu.cs;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String directory = args[0];     // default: "server-database"
        final int port = 5050;
        ServerSocket serverSocket = new ServerSocket(port);
        Socket socket = serverSocket.accept();
        DataInputStream dataInput = new DataInputStream(socket.getInputStream());
        ServerServices serverServices = new ServerServices(dataInput, directory);
        serverServices.firstStep();
        serverServices.secondStep();
        serverServices.closing();
    }
}
