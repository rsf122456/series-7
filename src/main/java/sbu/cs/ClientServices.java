package sbu.cs;
import java.io.*;

public class ClientServices {
    private final DataInputStream dataInput;
    private final DataOutputStream dataOutput;
    private final String filePath;
    private final int size;
    private byte[] bytes;
    public ClientServices(DataInputStream dataInput, DataOutputStream dataOutput, String filePath) throws IOException {
        this.dataInput = dataInput;
        this.dataOutput = dataOutput;
        this.filePath = filePath;
        size = dataInput.available();
    }
    public void firstStep() throws IOException {
        dataOutput.writeUTF(filePath);
        dataOutput.flush();
    }
    public void secondStep() throws IOException {
        dataOutput.writeInt(size);
        try {
            bytes = new byte[size];
            dataInput.readFully(bytes);
        } catch (Exception e){
            throw new IOException(e);
        }
    }
    public void thirdStep() throws IOException {
        dataOutput.write(bytes);
    }
}
