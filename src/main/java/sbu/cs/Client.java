package sbu.cs;
import java.io.*;
import java.net.*;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */

    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        final int port = 5050;
        Socket socket = new Socket("localhost", port);
        FileInputStream fileInput = new FileInputStream(filePath);
        DataInputStream dataInput = new DataInputStream(fileInput);
        DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
        ClientServices clientServices = new ClientServices(dataInput, dataOutput, filePath);
        clientServices.firstStep();
        clientServices.secondStep();
        clientServices.thirdStep();
        closing(socket, dataOutput);
    }
    private static void closing(Socket socket, DataOutputStream dataOutput) throws IOException {
        dataOutput.flush();
        dataOutput.close();
        socket.close();
    }
}
