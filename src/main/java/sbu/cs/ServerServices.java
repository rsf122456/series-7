package sbu.cs;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ServerServices {
    private final DataInputStream dataInput;
    private final FileOutputStream fileOutput;
    private int size;
    private byte[] bytes;
    public ServerServices(DataInputStream dataInput, String directory) throws IOException {
        this.dataInput = dataInput;
        fileOutput = new FileOutputStream(directory + "/" + dataInput.readUTF());
    }
    public void firstStep() throws IOException {
        size = dataInput.readInt();
        try {
            bytes = new byte[size];
            dataInput.readFully(bytes);
        } catch (Exception e){
            throw new IOException(e);
        }
    }
    public void secondStep() throws IOException {
        fileOutput.write(bytes);
    }
    public void closing() throws IOException {
        dataInput.close();
        fileOutput.flush();
        fileOutput.close();
    }
}
